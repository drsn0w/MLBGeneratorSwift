//
//  main.swift
//  MLBGenerator
//
//  Created by drsn0w on 6/9/15.
//  Copyright © 2015 drsn0w. All rights reserved.
//

import Foundation

let tttValues: [String] = ["200", "600", "403", "404", "405", "303", "108", "207", "609", "501", "306", "102", "701", "301", "501", "101", "300", "130", "100", "270", "310", "902", "104", "401", "902", "500", "700", "802"]
let ccValues: [String] = ["GU", "4N", "J9", "QX", "OP", "CD", "GU"]
let eeeeValues: [String] = ["DYWF", "F117", "F502", "F9GY", "F9H0", "F9H1", "F9H1", "DYWD", "F504", "F116", "F503", "F2FR", "F653", "F49P", "F651", "F49R", "F652", "DYW3", "F64V", "F0V5", "F64W", "FF4G", "FF4H", "FF4J", "FF4K", "FF4L", "FF4M", "FF4N", "FF4P", "DNY3", "DP00", "DJWK", "DM66", "DNJK", "DKG1", "DM65", "DNJJ", "DKG2", "DM67", "DNJL", "DMT3", "DMT5", "DJWN", "DM69", "DJWP", "DM6C"]
let kkValues: [String] = ["1H", "1M", "AD", "1F", "A8", "UE", "JA", "JC", "8C", "CB", "FB"]

print("MLBGenerator v1.0 - Written by TheRacerMaster, Ported to Swift 2 by drsn0w")
print("")


// Make an MLB
var ourMLB = MLB()

// Declare ioregistry
let ioregistry = IORegReader();

// Get the serial number, immutable
let computerSerial = ioregistry.getSerialNumber()

let jumpIndex = computerSerial.characters.count - 4
let lastFour = computerSerial.substringFromIndex(advance(computerSerial.startIndex, jumpIndex))
let appleYearCode = ourMLB.requestModel(lastFour)
print("Model:     " + appleYearCode)
print("Serial:    " + computerSerial)


// Set SSS to first three characters of serial number
ourMLB.sss = computerSerial.substringToIndex(advance(computerSerial.startIndex, 3))
// print("[DEBUG] SSS: " + ourMLB.sss)

// Get model year
let serialArray = Array(computerSerial.characters)
var manufactureYear = ""
let yearLetter = serialArray[3]
switch yearLetter {
    case "C", "D": ourMLB.modelYear = "0"; manufactureYear = "2010"
    case "F", "G": ourMLB.modelYear = "1"; manufactureYear = "2011"
    case "H", "J": ourMLB.modelYear = "2"; manufactureYear = "2012"
    case "K", "L": ourMLB.modelYear = "3"; manufactureYear = "2013"
    case "M", "N": ourMLB.modelYear = "4"; manufactureYear = "2014"
    case "P", "Q": ourMLB.modelYear = "5"; manufactureYear = "2015"
    case "R", "S": ourMLB.modelYear = "6"; manufactureYear = "2016"
    case "T", "V": ourMLB.modelYear = "7"; manufactureYear = "2017"
    case "W", "X": ourMLB.modelYear = "8"; manufactureYear = "2018"
    case "Y", "Z": ourMLB.modelYear = "9"; manufactureYear = "2019"
default: ourMLB.modelYear = "THIS MLB WILL NOT WORK"; manufactureYear = "error"
}
print("Mfr Year:  " + manufactureYear)

// Let's verify that I got this right...


// Generate a random week number ... update this later to do the actual correct shit pls
let weekNumber = Int(arc4random_uniform(51) + 1)
var weekString = String(weekNumber)
if weekString.characters.count == 1 { weekString = "0" + weekString}
ourMLB.modelWeek = weekString
print("Mfr Week:  " + ourMLB.modelWeek)
// print("[DEBUG] Week Code: " + ourMLB.modelWeek)

//let tttSelect = arc4random_uniform(tttValues.count)
//let ccSelect = arc4random_uniform(ccValues.count -1)
//let eeeeSelect =

let tttSelect = Int(arc4random_uniform(27))
let ccSelect = Int(arc4random_uniform(6))
let eeeeSelect = Int(arc4random_uniform(45))
let kkSelect = Int(arc4random_uniform(10))


ourMLB.ttt = tttValues[tttSelect]
ourMLB.cc = ccValues[ccSelect]
ourMLB.eeee = eeeeValues[eeeeSelect]
ourMLB.kk = kkValues[kkSelect]

print("TTT:       " +  ourMLB.ttt)
print("CC:        " + ourMLB.cc)
print("EEEE:      " + ourMLB.eeee)
print("KK:        " + ourMLB.kk)




print("")
print(" Final MLB Value")
print("=================")
print(ourMLB.getMLB())









