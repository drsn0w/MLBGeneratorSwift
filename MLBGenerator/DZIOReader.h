//
//  DZIOReader.h
//  MLBGenerator
//
//  Created by drsn0w on 6/10/15.
//  Copyright © 2015 drsn0w. All rights reserved.
//
#include <IOKit/IOKitLib.h>
#import <Foundation/Foundation.h>

@interface DZIOReader : NSObject

- (NSString *)getSerialNumber;

@end

