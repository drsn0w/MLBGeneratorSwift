//
//  MLB.swift
//  MLBGenerator
//
//  Created by Liam Crabbe on 6/10/15.
//  Copyright © 2015 drsn0w. All rights reserved.
//

import Foundation

extension String {
    func contains(find: String) -> Bool {
        return self.rangeOfString(find) != nil
    }
}

public class MLB {
    var sss = ""
    var modelYear = ""
    var modelWeek = ""
    var ttt = ""
    var cc = ""
    var eeee = ""
    var kk = ""
    
    func getMLB() -> String {
        return sss + modelYear + modelWeek + ttt + cc + eeee + kk
    }
    
    func requestModel(yearCode: String) -> String {
        let httpGetter: URLRequester = URLRequester()
        let responseString = httpGetter.getDataFrom("http://support-sp.apple.com/sp/product?cc=" + yearCode) as String
         //print("[DEBUG]" + responseString)
        
        var str = responseString.stringByReplacingOccurrencesOfString("<[^>]+>", withString: "", options: .RegularExpressionSearch, range: nil)
        let jumpIndex = str.characters.count - 5
        str = str.substringToIndex(advance(str.startIndex, jumpIndex))
        str = str.substringFromIndex(advance(str.startIndex,8))


        
        return str
        
        
    }
    
}