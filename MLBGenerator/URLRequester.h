//
//  URLRequester.h
//  MLBGenerator
//
//  Created by Liam Crabbe on 6/10/15.
//  Copyright © 2015 drsn0w. All rights reserved.
//

@interface URLRequester : NSObject


- (NSString *) getDataFrom:(NSString *)url;


@end